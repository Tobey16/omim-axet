package com.mapswithme.maps.scheduling;

import androidx.annotation.NonNull;

import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

public class FirebaseJobService
{
  private static final Logger LOGGER = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.MISC);
  private static final String TAG = FirebaseJobService.class.getSimpleName();

  @SuppressWarnings("NullableProblems")
  @NonNull
  private JobServiceDelegate mDelegate;
}
