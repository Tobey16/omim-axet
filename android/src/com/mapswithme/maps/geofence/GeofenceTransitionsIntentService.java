package com.mapswithme.maps.geofence;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.mapswithme.maps.LightFramework;
import com.mapswithme.maps.MwmApplication;
import com.mapswithme.maps.bookmarks.data.FeatureId;
import com.mapswithme.maps.location.LocationHelper;
import com.mapswithme.maps.location.LocationPermissionNotGrantedException;
import com.mapswithme.maps.scheduling.JobIdMap;
import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class GeofenceTransitionsIntentService extends JobIntentService
{
  private static final Logger LOG = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.MISC);
  private static final String TAG = GeofenceTransitionsIntentService.class.getSimpleName();
  private static final int LOCATION_PROBES_MAX_COUNT = 10;

  @NonNull
  private final Handler mMainThreadHandler = new Handler(Looper.getMainLooper());

  @Override
  protected void onHandleWork(@NonNull Intent intent)
  {
  }

  public static void enqueueWork(@NonNull Context context, @NonNull Intent intent)
  {
    int id = JobIdMap.getId(GeofenceTransitionsIntentService.class);
    enqueueWork(context, GeofenceTransitionsIntentService.class, id, intent);
    LOG.d(TAG, "Service was enqueued");
  }

  public static abstract class AbstractGeofenceTask implements Runnable
  {
    @NonNull
    private final MwmApplication mApplication;
    @NonNull
    private final GeofenceLocation mGeofenceLocation;

    AbstractGeofenceTask(@NonNull Application application,
                         @NonNull GeofenceLocation location)
    {
      mApplication = (MwmApplication)application;
      mGeofenceLocation = location;
    }

    @NonNull
    protected MwmApplication getApplication()
    {
      return mApplication;
    }

    @NonNull
    protected GeofenceLocation getGeofenceLocation()
    {
      Location lastKnownLocation = LocationHelper.INSTANCE.getLastKnownLocation();
      return lastKnownLocation == null ? mGeofenceLocation
                                       : GeofenceLocation.from(lastKnownLocation);
    }
  }
}
