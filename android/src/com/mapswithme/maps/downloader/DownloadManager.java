package com.mapswithme.maps.downloader;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.preference.PreferenceManager;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.wget.ApacheIndex;
import com.github.axet.wget.RangeSet;
import com.mapswithme.maps.Framework;
import com.mapswithme.util.NetworkPolicy;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadManager {
    public static final String TAG = DownloadManager.class.getSimpleName();

    public static String MAPS = "http://download.openstreetmap.ru/mapsme/";
    public static String LEGACY = "https://gitlab.com/axet/omim/-/raw/master/data/";
    public static SimpleDateFormat DATE = new SimpleDateFormat("yyMMdd", Locale.US);
    public static String PREF_VERSION = "apache_version";

    // https://github.com/osmandapp/Osmand/blob/master/OsmAnd-java/src/main/java/net/osmand/util/MapUtils.java#L310
    public static String OSM_URL = "http://osm.org/go/";
    public static final char intToBase64[] = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', '~'
    };

    public static DownloadManager DM = new DownloadManager();

    public Handler handler = new Handler(Looper.getMainLooper());
    public Context context;
    public ApacheIndex index;
    public HashMap<String, Master> map = new HashMap<>();
    public Runnable update;
    public Executor executors = Executors.newFixedThreadPool(4);
    public Runnable init = new Runnable() {
        @Override
        public void run() {
            if (context != null) {
                long v = PreferenceManager.getDefaultSharedPreferences(context).getLong(PREF_VERSION, 0);
                if (v > Framework.nativeGetDataVersion())
                    Framework.nativeSetDataVersion(v);
            }
        }
    };
    public Runnable save;

    public static String filter(String url) {
        if (url.isEmpty())
            return url;
        if (url.startsWith("http"))
            return url;
        if (url.endsWith(".ttf") || url.startsWith("World"))
            return LEGACY + url;
        String re = "\\/(\\d+)\\/(.*)";
        Pattern p = Pattern.compile(re);
        Matcher m = p.matcher(url);
        if (m.matches())
            url = m.group(2);
        return MAPS + url;
    }

    public static long interleaveBits(long x, long y) {
        long c = 0;
        for (byte b = 31; b >= 0; b--) {
            c = (c << 1) | ((x >> b) & 1);
            c = (c << 1) | ((y >> b) & 1);
        }
        return c;
    }

    public static String createShortLinkString(double latitude, double longitude, int zoom) {
        long lat = (long) (((latitude + 90d) / 180d) * (1L << 32));
        long lon = (long) (((longitude + 180d) / 360d) * (1L << 32));
        long code = interleaveBits(lon, lat);
        String str = "";
        // add eight to the zoom level, which approximates an accuracy of one pixel in a tile.
        for (int i = 0; i < Math.ceil((zoom + 8) / 3d); i++) {
            str += intToBase64[(int) ((code >> (58 - 6 * i)) & 0x3f)];
        }
        // append characters onto the end of the string to represent
        // partial zoom levels (characters themselves have a granularity of 3 zoom levels).
        for (int j = 0; j < (zoom + 8) % 3; j++) {
            str += '-';
        }
        return str;
    }

    public class Master extends ChunkTask { // Master (First request) Chunk
        public ArrayList<ChunkTask> deps = new ArrayList<>();
        public int done = 0;
        public RangeSet ranges;

        public Master(long httpCallbackID, String url, long beg, long end, long expectedFileSize, byte[] postBody, String userAgent) {
            super(httpCallbackID, url, beg, end, expectedFileSize, postBody, userAgent);
        }

        @Override
        protected void onProgressUpdate(byte[]... data) {
            super.onProgressUpdate(data);
            purge();
        }

        @Override
        public void onFinish(long id, int err, long beg, long end) {
            done = err;
            for (ChunkTask k : deps)
                done(k);
        }

        public void purge() {
            long beg = mBeg;
            long end = beg + mDownloadedBytes - 1;
            for (int i = 0; i < deps.size(); i++) {
                ChunkTask d = deps.get(i);
                if (beg <= d.mBeg && d.mEnd <= end && d.mEnd != d.mExpectedFileSize - 1) // never purge last part
                    finish(d, HttpURLConnection.HTTP_OK);
            }
        }

        public void depends(ChunkTask t) {
            deps.add(t);
            if (ranges == null)
                ranges = new RangeSet(t.mBeg, t.mExpectedFileSize - 1);
            ranges.exclude(t.mBeg, t.mEnd);
            if (done != 0)
                done(t);
        }

        public void finish(ChunkTask t, final int done) {
            deps.remove(t);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ChunkTask.nativeOnFinish(t.mHttpCallbackID, done, t.mBeg, t.mEnd);
                }
            });
        }

        public void done(ChunkTask t) {
            finish(t, done);
            check();
        }

        public void check() {
            if (ranges.isEmpty())
                map.remove(mUrl);
        }
    }

    public DownloadManager() {
        update = new Runnable() {
            @Override
            public void run() {
                Thread thread = new Thread("Download maps Thread") {
                    @Override
                    public void run() {
                        try {
                            update(false);
                        } catch (IOException e) {
                            Log.d(TAG, "unable to download maps", e);
                        }
                    }
                };
                thread.start();
            }
        };
        if (!NetworkPolicy.getCurrentNetworkUsageStatus()) {
            handler.post(init);
        } else {
            update.run();
        }
    }

    synchronized public void init(Context context) {
        this.context = context;
        if (index == null)
            init.run(); // not initialized yet, restore version
        else
            update.run(); // index downloaded before init, save version
    }

    synchronized public void update(boolean force) throws IOException {
        long now = System.currentTimeMillis();
        if (force || index == null || index.last + 12 * AlarmManager.HOUR1 < now) {
            Log.d(TAG, "Start grabbing: " + MAPS);
            index = new ApacheIndex(MAPS);
            int count = 0;
            long a = 0;
            for (String key : index.keySet()) {
                ApacheIndex.Entry v = index.get(key);
                if (v.date != null) {
                    a += v.date.getTime();
                    count++;
                }
            }
            if (count == 0) {
                index = null;
                throw new IOException("Maps index malformed, check your internet connection");
            }
            final long version = Long.valueOf(DATE.format(new Date(a / count)));
            Log.d(TAG, "Grabbed maps version: " + version);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (Framework.nativeGetDataVersion() != version) {
                        Framework.nativeSetDataVersion(version);
                        save = new Runnable() {
                            @Override
                            public void run() {
                                PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(PREF_VERSION, version).commit();
                            }
                        };
                        if (context != null) {
                            save.run();
                            save = null;
                        }
                    }
                }
            });
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (context != null && save != null) {
                    save.run();
                    save = null;
                }
            }
        });
        handler.removeCallbacks(update);
        handler.postDelayed(update, 1 * AlarmManager.HOUR1);
    }

    synchronized public boolean start(ChunkTask k) {
        if (index == null)
            return false;
        String url = DownloadManager.filter(k.mUrl);
        url = com.github.axet.androidlibrary.net.HttpClient.safe(url);
        DownloadManager.Master t = map.get(url);
        if (t == null) {
            Log.d(TAG, "download url: " + url);
            ApacheIndex.Entry v = index.get(new File(k.mUrl).getName());
            long size = k.mExpectedFileSize;
            if (v != null)
                size = v.size;
            if (url.startsWith(LEGACY))
                size = 0;
            t = new DownloadManager.Master(k.mHttpCallbackID, url, k.mBeg, -1, size, null, k.mUserAgent);
            map.put(url, t);
            t.executeOnExecutor(executors, (Void[]) null);
        }
        t.depends(k);
        return true;
    }

    synchronized public void cancel(ChunkTask t) {
        for (String k : new HashSet<>(map.keySet())) {
            Master v = map.get(k);
            if (v.deps.contains(t)) {
                if (v.done == 0) {
                    v.cancel(false);
                    map.remove(k);
                }
            }
        }
    }
}
